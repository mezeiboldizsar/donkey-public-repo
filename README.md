# Donkey

# What is Donkey?
Donkey is a budget management application that lets you track and share your expenses with others.

# Key features:
- Create a budget and add/remove members on demand
- Create an account and add/remove members on demand
- Track incomes/expenses
- Offline first support

# Software stack
- Android frontend: Kotlin
- Firebase Firestore
- Firebase Cloud functions

# Why have I created Donkey?
I have two reasons for creating it. First, I wanted to learn and play around with technologies that I do not use during my day to day job. Second, I believe that any craftsman should use at least one thing that was created by himself.

# Copyright
I've made this repository public because I want to learn so I am open for any feedback regarding the code but I reserve all rights for the source code.
I know that people think that what is on the internet is free and I can not stop anyone from copying the source code, but if you do so at leaste please donate. Thanks!  
[![Donate](https://cdn.rawgit.com/twolfson/paypal-github-button/1.0.0/dist/button.svg)](https://paypal.me/boldizsarmezei)