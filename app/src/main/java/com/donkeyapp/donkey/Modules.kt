package com.donkeyapp.donkey

import androidx.lifecycle.MutableLiveData
import com.donkeyapp.donkey.store.firebase.repository.*
import com.donkeyapp.donkey.ui.account.detail.AccountDetailViewModel
import com.donkeyapp.donkey.ui.account.detail.AccountDetailViewModelImpl
import com.donkeyapp.donkey.ui.account.detail.AccountValidator
import com.donkeyapp.donkey.ui.account.list.AccountListViewModel
import com.donkeyapp.donkey.ui.account.list.AccountListViewModelImpl
import com.donkeyapp.donkey.ui.budget.detail.BudgetDetailViewModel
import com.donkeyapp.donkey.ui.budget.detail.BudgetDetailViewModelImpl
import com.donkeyapp.donkey.ui.budget.detail.BudgetValidator
import com.donkeyapp.donkey.ui.budget.list.BudgetListViewModel
import com.donkeyapp.donkey.ui.budget.list.BudgetListViewModelImpl
import com.donkeyapp.donkey.ui.common.MemberViewModel
import com.donkeyapp.donkey.ui.common.MemberViewModelImpl
import com.donkeyapp.donkey.ui.common.UserSearchViewModel
import com.donkeyapp.donkey.ui.common.UserSearchViewModelImpl
import com.donkeyapp.donkey.ui.transaction.detail.Direction
import com.donkeyapp.donkey.ui.transaction.detail.TransactionDetailViewModel
import com.donkeyapp.donkey.ui.transaction.detail.TransactionDetailViewModelImpl
import com.donkeyapp.donkey.ui.transaction.list.TransactionListViewModel
import com.donkeyapp.donkey.ui.transaction.list.TransactionListViewModelImpl
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {

    private val validatorModule = module {
        single { AccountValidator() }
        single { BudgetValidator() }
    }

    private val firebaseModule = module {
        single { FirestoreUserRepository() }
        single { FirestoreBudgetRepository() }
        single { FirestoreAccountRepository() }
        single { FirestoreIncomeRepository() }
        single { FirestoreExpenseRepository() }
    }

    private val viewModelModule = module {
        viewModel<TransactionListViewModel> { TransactionListViewModelImpl(get(), get()) }
        viewModel<TransactionDetailViewModel> { (transactionId: String?, direction: Direction) ->
            TransactionDetailViewModelImpl(transactionId, direction, get(), get(), get(), get())
        }
        viewModel<BudgetDetailViewModel> { (budgetId: String?) ->
            BudgetDetailViewModelImpl(budgetId, get<BudgetValidator>(), get())
        }
        viewModel<BudgetListViewModel> { BudgetListViewModelImpl(get(), get()) }
        viewModel<AccountListViewModel> { AccountListViewModelImpl(get(), get(), get(), get()) }
        viewModel<AccountDetailViewModel> { (accountId: String?) ->
            AccountDetailViewModelImpl(accountId, get<AccountValidator>(), get(), get(), get())
        }
        viewModel<MemberViewModel> { (roles: MutableLiveData<MutableMap<String, Int>>) ->
            MemberViewModelImpl(roles, get())
        }
        viewModel<UserSearchViewModel> { (onAdd: (String) -> Unit) ->
            UserSearchViewModelImpl(onAdd, get())
        }
    }

    val appModules = listOf(
        validatorModule,
        viewModelModule,
        firebaseModule
    )
}