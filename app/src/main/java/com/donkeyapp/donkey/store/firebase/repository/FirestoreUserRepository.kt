package com.donkeyapp.donkey.store.firebase.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.donkeyapp.donkey.store.firebase.FirestoreUtils.db
import com.donkeyapp.donkey.store.firebase.currentUser
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.User
import com.donkeyapp.donkey.utils.firebase.onSimpleQueryCompleteListener
import com.donkeyapp.donkey.utils.firebase.toLiveData
import com.google.firebase.firestore.FieldPath

abstract class FirestoreAbstractUserRepository : AbstractBaseCollectionFirestoreRepository<User>(
    "user", User::class.java
) {
    abstract fun initCurrentUserIfFirstTime(onComplete: () -> Unit)
    abstract fun findMany(ids: List<String>): LiveData<List<Entity<User>>>
    abstract fun findByName(name: String, onComplete: (List<Entity<User>>) -> Unit)
}

class FirestoreUserRepository : FirestoreAbstractUserRepository() {

    override fun initCurrentUserIfFirstTime(onComplete: () -> Unit) {
        val docRef = db.document("users/${currentUserId()}")
        docRef.get().addOnCompleteListener {
            if (it.result!!.exists()) { //TODO it.result can throw exception
                onComplete()
                return@addOnCompleteListener
            }
            docRef.set(User(name = currentUser().displayName ?: ""))
                .addOnSuccessListener {
                    onComplete()
                }
                .addOnFailureListener {
                    //TODO replace this as this crashes the app
                    throw IllegalStateException("Could not save user.")
                }
        }
    }

    override fun findMany(ids: List<String>): LiveData<List<Entity<User>>> {
        return if (ids.isEmpty()) {
            MutableLiveData<List<Entity<User>>>().apply {
                postValue(listOf())
            }
        } else {
            db.collection("users")
                .whereIn(FieldPath.documentId(), ids)
                .toLiveData(clazz)
        }
    }

    override fun findByName(name: String, onComplete: (List<Entity<User>>) -> Unit) {
        db.collection("users")
            .orderBy("name")
            .startAt(name)
            .endAt(name + "\uf8ff")
            .limit(15)
            .get()
            .onSimpleQueryCompleteListener(User::class.java) { users ->
                onComplete(users.filter { it.id != currentUserId() })
            }
    }

    override fun update(entity: Entity<User>): String {
        //TODO implement
        return entity.id
    }

}