package com.donkeyapp.donkey.store.firebase.model

import com.donkeyapp.donkey.store.firebase.currentUser
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.google.firebase.Timestamp
import org.threeten.bp.LocalDate

data class TransactionUser constructor(
    val id: String = currentUserId(),
    val name: String = currentUser().displayName ?: ""
)

data class TransactionAccount constructor(
    val id: String = "",
    val name: String = ""
)

data class TransactionBudget constructor(
    val id: String = "",
    val name: String = "",
    var category: TransactionBudgetCategory? = null
)

data class TransactionBudgetCategory constructor(
    val id: String = "",
    val name: String = ""
)

data class Transaction constructor(
    var user: TransactionUser = TransactionUser(),
    var account: TransactionAccount? = null,
    var budget: TransactionBudget? = null,
    var comment: String? = null,
    var date: Timestamp = Timestamp.now(),
    var money: Money = Money(),
    var roles: Map<String, Int> = mapOf(),
    var userIds: List<String> = listOf(),
    var year: Int = LocalDate.now().year,
    var month: Int = LocalDate.now().monthValue
)