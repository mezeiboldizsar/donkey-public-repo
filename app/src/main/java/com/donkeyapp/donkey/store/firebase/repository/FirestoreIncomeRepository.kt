package com.donkeyapp.donkey.store.firebase.repository

import com.donkeyapp.donkey.store.firebase.model.Transaction

class FirestoreIncomeRepository :
    FirestoreTransactionRepository("incomes", Transaction::class.java)