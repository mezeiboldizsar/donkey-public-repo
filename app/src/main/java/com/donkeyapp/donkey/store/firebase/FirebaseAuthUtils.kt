package com.donkeyapp.donkey.store.firebase

import android.content.Context
import com.donkeyapp.donkey.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

private val instance = FirebaseAuth.getInstance()

fun currentUser(): FirebaseUser =
    instance.currentUser ?: throw IllegalStateException("User should be signed in.")

fun currentUserId(): String =
    instance.currentUser?.uid ?: throw IllegalStateException("User should be signed in.")

private fun gso(context: Context) = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
    .requestIdToken(context.getString(R.string.default_web_client_id))
    .requestEmail()
    .build()

fun googleSignInClient(context: Context): GoogleSignInClient =
    GoogleSignIn.getClient(context, gso(context))
