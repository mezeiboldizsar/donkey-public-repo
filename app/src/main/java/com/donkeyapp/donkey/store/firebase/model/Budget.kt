package com.donkeyapp.donkey.store.firebase.model

data class Budget constructor(
    var name: String = "",
    override val roles: MutableMap<String, Int> = mutableMapOf(),
    var userIds: List<String> = listOf(),
    val categories: MutableMap<String, Category> = mutableMapOf()
) : WithRoles


data class Category constructor(
    var name: String = ""
)