package com.donkeyapp.donkey.store.firebase.repository

import androidx.lifecycle.LiveData
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Account
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.utils.firebase.toLiveData
import com.donkeyapp.donkey.utils.firebase.toObjectWithId
import com.google.firebase.firestore.Source

class FirestoreAccountRepository : AbstractBaseCollectionFirestoreRepository<Account>(
    "accounts", Account::class.java
) {

    override fun findAll(): LiveData<List<Entity<Account>>> =
        collection.whereArrayContains("userIds", currentUserId()).toLiveData(clazz)

    override fun update(entity: Entity<Account>): String {
        val doc = document(entity.id)
        doc.get(Source.CACHE).addOnSuccessListener {
            val current = it.toObjectWithId(clazz)
            if (entity.data.name != current?.data?.name) {
                doc.update("name", entity.data.name)
            }
            if (entity.data.money != current?.data?.money) {
                doc.update("money", entity.data.money)
            }
            if (entity.data.roles != current?.data?.roles) {
                doc.update("roles", entity.data.roles)
            }
            if(entity.data.userIds != current?.data?.userIds) {
                doc.update("userIds", entity.data.userIds)
            }
        }
        return entity.id
    }

}