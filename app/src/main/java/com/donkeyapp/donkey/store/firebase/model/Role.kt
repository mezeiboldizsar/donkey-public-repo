package com.donkeyapp.donkey.store.firebase.model

enum class Role {
    NONE, READ, WRITE, ADMIN, OWNER;

    companion object {
        fun get(ordinal: Int): Role = values()[ordinal]
    }

}