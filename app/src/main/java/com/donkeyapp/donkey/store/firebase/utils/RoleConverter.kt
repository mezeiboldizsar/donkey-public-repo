package com.donkeyapp.donkey.store.firebase.utils

import com.donkeyapp.donkey.store.firebase.model.Role
import com.google.gson.*
import java.lang.reflect.Type

class RoleConverter : JsonSerializer<Role>, JsonDeserializer<Role> {
    override fun serialize(
        src: Role?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement = JsonPrimitive(src!!.ordinal.toString())


    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Role = Role.values()[json!!.toString().toInt()]

}