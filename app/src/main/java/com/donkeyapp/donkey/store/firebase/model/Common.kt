package com.donkeyapp.donkey.store.firebase.model

data class Entity<T>(
    val id: String = "",
    val data: T
)

data class Money(
    var amount: Double = 0.0,
    var currency: String = ""
)

interface WithRoles {

    val roles: Map<String, Int>

    fun ownerId(): String? = roles.filterValues {
        it == Role.OWNER.ordinal
    }.keys.firstOrNull()

    fun role(userId: String): Int = roles[userId] ?: 0

}

fun List<WithRoles>.ownerIds(): List<String> = mapNotNull { it.ownerId() }.distinct()