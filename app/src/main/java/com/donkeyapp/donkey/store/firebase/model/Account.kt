package com.donkeyapp.donkey.store.firebase.model

data class Account constructor(
    var name: String = "",
    var money: Money = Money(),
    override val roles: MutableMap<String, Int> = mutableMapOf(),
    var userIds: List<String> = listOf()
) : WithRoles