package com.donkeyapp.donkey.store.firebase.model

data class User constructor(
    var name: String = "",
    var photo: String? = null
)