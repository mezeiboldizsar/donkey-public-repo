package com.donkeyapp.donkey.store.firebase.repository

import androidx.lifecycle.LiveData
import com.donkeyapp.donkey.store.firebase.FirestoreUtils.db
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.utils.firebase.toLiveData

interface BaseCollectionFirestoreRepository<T> {
    fun findOne(entityId: String): LiveData<Entity<T>?>
    fun findAll(): LiveData<List<Entity<T>>>
    fun create(entity: T): String
    fun update(entity: Entity<T>): String
    fun delete(entityId: String)
}

abstract class AbstractBaseCollectionFirestoreRepository<T : Any>(
    private val basePath: String,
    protected val clazz: Class<T>
) : BaseCollectionFirestoreRepository<T> {

    protected val collection = db.collection(basePath)

    override fun findOne(entityId: String): LiveData<Entity<T>?> =
        document(entityId).toLiveData(clazz)

    override fun findAll(): LiveData<List<Entity<T>>> = collection.toLiveData(clazz)

    override fun create(entity: T): String {
        val doc = collection.document()
        doc.set(entity)
        return doc.id
    }

    override fun delete(entityId: String) {
        document(entityId).delete()
    }

    protected fun document(id: String) = db.document("$basePath/$id")

}
