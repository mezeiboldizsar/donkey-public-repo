package com.donkeyapp.donkey.store.firebase.repository

import com.donkeyapp.donkey.store.firebase.model.Transaction

class FirestoreExpenseRepository :
    FirestoreTransactionRepository("expenses", Transaction::class.java)