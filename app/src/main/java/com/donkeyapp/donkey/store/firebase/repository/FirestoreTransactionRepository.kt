package com.donkeyapp.donkey.store.firebase.repository

import androidx.lifecycle.LiveData
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Transaction
import com.donkeyapp.donkey.utils.firebase.await
import com.donkeyapp.donkey.utils.firebase.toLiveData
import com.donkeyapp.donkey.utils.firebase.toObjectWithId
import com.google.firebase.firestore.Source

abstract class FirestoreTransactionRepository(
    basePath: String, clazz: Class<Transaction>
) : AbstractBaseCollectionFirestoreRepository<Transaction>(basePath, clazz) {

    fun findAll(
        year: Int,
        month: Int,
        budgetId: String? = null
    ): LiveData<List<Entity<Transaction>>> {
        val query = collection
            .whereArrayContains("userIds", currentUserId())
            .whereEqualTo("year", year)
            .whereEqualTo("month", month)
        if (budgetId != null) {
            query.whereEqualTo("budgetId", budgetId)
        }
        return query.toLiveData(clazz)
    }

    override fun update(entity: Entity<Transaction>): String {
        val doc = document(entity.id)
        doc.get(Source.CACHE).addOnSuccessListener {
            val current = it.toObjectWithId(clazz)
            if (entity.data.account != current?.data?.account) {
                doc.update("account", entity.data.account)
            }
            if (entity.data.budget != current?.data?.budget) {
                doc.update("budget", entity.data.budget)
            }
            if (entity.data.comment != current?.data?.comment) {
                doc.update("comment", entity.data.comment)
            }
            if (entity.data.date != current?.data?.date) {
                doc.update("date", entity.data.date)
            }
            if (entity.data.money != current?.data?.money) {
                doc.update("money", entity.data.money)
            }
            if(entity.data.year != current?.data?.year) {
                doc.update("year", entity.data.year)
            }
            if(entity.data.month != current?.data?.month) {
                doc.update("month", entity.data.month)
            }
        }
        return entity.id
    }

    suspend fun sum(accountId: String): Double =
        collection.whereEqualTo("account.id", accountId).get().await(clazz).map {
            it.data.money.amount
        }.sum()

}