package com.donkeyapp.donkey.store.firebase

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings

object FirestoreUtils {

    private val settings = FirebaseFirestoreSettings.Builder()
        .setPersistenceEnabled(true)
        .build()

    val db by lazy {
        val db = FirebaseFirestore.getInstance()
        db.firestoreSettings = settings
        db
    }

}