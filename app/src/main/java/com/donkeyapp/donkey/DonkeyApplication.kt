package com.donkeyapp.donkey

import android.app.Application
import androidx.databinding.DataBindingUtil
import com.donkeyapp.donkey.Modules.appModules
import com.donkeyapp.donkey.bindings.BindingComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DonkeyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        startKoin {
            androidLogger()
            androidContext(this@DonkeyApplication)
            modules(appModules)
        }
        DataBindingUtil.setDefaultComponent(BindingComponent())
    }
    
}