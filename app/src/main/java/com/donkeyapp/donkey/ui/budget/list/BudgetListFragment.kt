package com.donkeyapp.donkey.ui.budget.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.FragmentBudgetListBinding
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.ui.common.RecyclerViewWrapper
import kotlinx.android.synthetic.main.fragment_budget_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class BudgetListFragment :
    BindableScopedFragment<FragmentBudgetListBinding>(
        R.layout.fragment_budget_list
    ) {

    private val viewModel by viewModel<BudgetListViewModel>()

    override fun bindUI() {
        initBudgetList()
        binding.viewModel = viewModel
        binding.lifecycleOwner = this@BudgetListFragment
        addBtn.setOnClickListener {
            openAccountDetailFragment(null)
        }
    }

    private fun initBudgetList() {
        val budgetsRecyclerView = RecyclerViewWrapper(budgetsRV, this)
        budgetsRecyclerView.groupAdapter.setOnItemLongClickListener { item, _ ->
            (item as? BudgetListItemView)?.let {
                openAccountDetailFragment(it.id)
            }
            true
        }
    }

    private fun openAccountDetailFragment(budgetId: String?) {
        val actionDetail = BudgetListFragmentDirections.actionToBudgetDetailFragment(budgetId)
        navigate(actionDetail)
    }

}
