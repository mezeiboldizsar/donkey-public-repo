package com.donkeyapp.donkey.ui.common

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemSearchUserListItemBinding
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.User
import com.xwray.groupie.databinding.BindableItem

class UserItemView(
    private val onAdd: (String) -> Unit,
    private val user: Entity<User>
) : BindableItem<ItemSearchUserListItemBinding>() {

    override fun getLayout() = R.layout.item_search_user_list_item

    override fun bind(binding: ItemSearchUserListItemBinding, pos: Int) {
        binding.user = user.data
        binding.addBtn.setOnClickListener {
            onAdd(user.id)
        }
    }

}