package com.donkeyapp.donkey.ui.budget.list

import androidx.lifecycle.*
import com.donkeyapp.donkey.store.firebase.model.ownerIds
import com.donkeyapp.donkey.store.firebase.repository.FirestoreBudgetRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreUserRepository

abstract class BudgetListViewModel : ViewModel() {
    abstract val budgetListItems: LiveData<List<BudgetListItemView>>
}

class BudgetListViewModelImpl(
    budgetRepository: FirestoreBudgetRepository,
    private val userRepository: FirestoreUserRepository
) : BudgetListViewModel() {

    private val budgets = budgetRepository.findAll()

    private val users = budgets.switchMap { budgets ->
        userRepository.findMany(budgets.map { it.data }.ownerIds())
    }

    override val budgetListItems = budgets.switchMap { budgets ->
        users.map { users ->
            val userMap = users.groupBy { it.id }
            budgets.map {
                BudgetListItemView(it.id, it.data, userMap[it.data.ownerId()]?.firstOrNull()?.data)
            }
        }
    }

}
