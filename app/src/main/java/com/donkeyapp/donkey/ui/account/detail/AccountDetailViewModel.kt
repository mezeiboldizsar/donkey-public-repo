package com.donkeyapp.donkey.ui.account.detail

import androidx.lifecycle.*
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Account
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.repository.FirestoreAccountRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreExpenseRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreIncomeRepository
import com.donkeyapp.donkey.ui.common.BaseViewModel
import com.donkeyapp.donkey.utils.*

abstract class AccountDetailViewModel : BaseViewModel() {

    protected abstract val validator: Validator<Account>

    abstract val account: LiveData<Entity<Account>>

    abstract fun save()

    abstract fun delete()

    fun isValidName(name: String) = name.isAccountName().also { validate() }

    fun isValidAmount(amount: String) = amount.isValidAmount().also { validate() }

    private fun validate() {
        state.postValue(SaveState.isSavable(validator.validate(account.value?.data)))
    }

}

class AccountDetailViewModelImpl(
    private val accountId: String?,
    override val validator: Validator<Account>,
    private val accountRepository: FirestoreAccountRepository,
    private val incomeRepository: FirestoreIncomeRepository,
    private val expenseRepository: FirestoreExpenseRepository
) : AccountDetailViewModel() {

    private val newAccount by lazy {
        Entity(data = Account(roles = mutableMapOf(currentUserId() to Role.OWNER.ordinal)))
    }

    private var incomeSum = 0.0
    private var expenseSum = 0.0

    override val account = liveData {
        if (accountId == null) {
            state.postValue(Unsavable)
            emit(newAccount)
        } else {
            incomeSum = incomeRepository.sum(accountId)
            expenseSum = expenseRepository.sum(accountId)
            emitSource(accountRepository.findOne(accountId).map {
                state.postValue(SaveState.isSavable(it != null))
                val account = it ?: newAccount
                account.data.money.amount += (incomeSum - expenseSum)
                account
            })
        }
    }

    override fun save() {
        val value = account.value ?: return
        value.data.userIds = value.data.roles.keys.toList()
        state.postValue(Saving)
        if (accountId == null) {
            accountRepository.create(value.data)
        } else {
            value.data.money.amount -= (incomeSum - expenseSum)
            accountRepository.update(value)
        }
        state.postValue(Saved)
    }

    override fun delete() {
        if (accountId != null) {
            state.postValue(Saving)
            accountRepository.delete(accountId)
        }
        state.postValue(Saved)
    }

}