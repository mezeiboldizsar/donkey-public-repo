package com.donkeyapp.donkey.ui.common

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder

class RecyclerViewWrapper(
    recyclerView: RecyclerView,
    fragment: Fragment
) {

    val groupAdapter = GroupAdapter<ViewHolder>()

    init {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(fragment.context)
            adapter = groupAdapter
        }
    }

}