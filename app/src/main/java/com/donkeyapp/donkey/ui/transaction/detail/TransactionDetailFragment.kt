package com.donkeyapp.donkey.ui.transaction.detail

import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.FragmentTransactionDetailBinding
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.utils.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class TransactionDetailFragment :
    BindableScopedFragment<FragmentTransactionDetailBinding>(
        R.layout.fragment_transaction_detail
    ) {

    private val viewModel by inject<TransactionDetailViewModel> {
        val args = arguments?.let { TransactionDetailFragmentArgs.fromBundle(it) }
        parametersOf(args?.transactionId, args?.direction)
    }

    override fun bindUI() {
        binding.fragment = this
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.state.observe(this, Observer {
            if (it == Saved) {
                Navigation.findNavController(view ?: return@Observer).popBackStack()
            }
        })
    }

    fun openDatePicker() {
        context?.datePicker(viewModel.transaction.value?.data?.date?.toLocalDate()) {
            viewModel.setDate(it, null)
        }
    }

    fun openTimePicker() {
        context?.timePicker(viewModel.transaction.value?.data?.date?.toLocalTime()) {
            viewModel.setDate(null, it)
        }
    }

    fun onItemSelect(item: SpinnerItem<Any>?) {
        (item as? AccountSpinnerItem)?.let {
            viewModel.setAccount(it)
        }
        (item as? BudgetSpinnerItem)?.let {
            viewModel.setBudget(it)
        }
        (item as? CategorySpinnerItem)?.let {
            viewModel.setCategory(it)
        }
    }

}