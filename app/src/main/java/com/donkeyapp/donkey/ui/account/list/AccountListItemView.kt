package com.donkeyapp.donkey.ui.account.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemAccountListBinding
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Account
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.model.User
import com.donkeyapp.donkey.utils.getString
import com.xwray.groupie.databinding.BindableItem

class AccountListItemView(
    val id: String,
    private val account: Account,
    private val owner: User?
) : BindableItem<ItemAccountListBinding>() {

    override fun getLayout(): Int = R.layout.item_account_list

    override fun bind(binding: ItemAccountListBinding, pos: Int) {
        binding.account = account
        binding.owner = owner
        binding.roleTextView.text =
            binding.roleTextView.context.getString(Role.get(account.role(currentUserId())).toString())
    }

}