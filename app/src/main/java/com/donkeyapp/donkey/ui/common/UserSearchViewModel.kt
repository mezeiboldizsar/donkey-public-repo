package com.donkeyapp.donkey.ui.common

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.User
import com.donkeyapp.donkey.store.firebase.repository.FirestoreUserRepository

abstract class UserSearchViewModel : ViewModel() {
    abstract val userItems: LiveData<List<UserItemView>>

    abstract fun searchUsers(emailOrName: String)
}

class UserSearchViewModelImpl(
    private val onAdd: (String) -> Unit,
    private val userRepository: FirestoreUserRepository
) : UserSearchViewModel() {

    private val users = MutableLiveData<List<Entity<User>>>()

    override val userItems = users.map { users ->
        users.map { UserItemView(onAdd, it) }
    }

    override fun searchUsers(emailOrName: String) {
        if (emailOrName.length > 2) {
            userRepository.findByName(emailOrName) {
                users.postValue(it)
            }
        }
    }

}