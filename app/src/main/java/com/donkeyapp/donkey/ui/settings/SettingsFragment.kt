package com.donkeyapp.donkey.ui.settings


import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.store.firebase.googleSignInClient
import com.donkeyapp.donkey.ui.SignInActivity
import com.firebase.ui.auth.AuthUI
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.support.v4.intentFor

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferneces)
        (findPreference("logout") as Preference?)?.onPreferenceClickListener =
            Preference.OnPreferenceClickListener {
                googleSignInClient(this.context!!)?.signOut()
                AuthUI.getInstance().signOut(this.context!!).addOnSuccessListener {
                    startActivity(intentFor<SignInActivity>().newTask().clearTask())
                }
                true
            }
    }

}
