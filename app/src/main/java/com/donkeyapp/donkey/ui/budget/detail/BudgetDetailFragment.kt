package com.donkeyapp.donkey.ui.budget.detail

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import androidx.navigation.Navigation
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.DialogCategoryDetailBinding
import com.donkeyapp.donkey.databinding.FragmentBudgetDetailBinding
import com.donkeyapp.donkey.store.firebase.model.Category
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.ui.common.MemberViewModel
import com.donkeyapp.donkey.ui.common.RecyclerViewWrapper
import com.donkeyapp.donkey.ui.common.UserSearchViewModel
import com.donkeyapp.donkey.utils.Saved
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.dialog_category_detail.*
import kotlinx.android.synthetic.main.fragment_budget_detail.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.util.*

class BudgetDetailFragment : BindableScopedFragment<FragmentBudgetDetailBinding>(
    R.layout.fragment_budget_detail
) {

    private val args by lazy {
        arguments?.let { BudgetDetailFragmentArgs.fromBundle(it) }
    }

    val budgetViewModel by inject<BudgetDetailViewModel> { parametersOf(args?.budgetId) }

    val memberViewModel by inject<MemberViewModel> {
        parametersOf(budgetViewModel.budget.map {
            it.data.roles
        })
    }

    val searchViewModel by inject<UserSearchViewModel> {
        parametersOf({ userId: String ->
            memberViewModel.add(userId)
            hideSearchResultRecyclerView()
        })
    }

    val category = MutableLiveData<Pair<String, Category>>()

    override fun bindUI() {
        RecyclerViewWrapper(membersRecyclerView, this)
        RecyclerViewWrapper(searchResultRecyclerView, this)
        initCategoryList()
        binding.fragment = this
        binding.isEditMode = args?.budgetId != null
        binding.lifecycleOwner = this
        addCategoryBtn.setOnClickListener {
            category.postValue(UUID.randomUUID().toString() to Category())
            initCategoryDialog()
        }
        budgetViewModel.state.observe(this, Observer {
            if (it == Saved) {
                Navigation.findNavController(view ?: return@Observer).popBackStack()
            }
        })
    }

    private fun initCategoryList() {
        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            setOnItemLongClickListener { item, _ ->
                (item as? CategoryItemView)?.let {
                    category.postValue(it.category)
                    initCategoryDialog()
                }
                true
            }
        }
        categoriesRV.apply {
            layoutManager = FlowLayoutManager().apply { isAutoMeasureEnabled = true }
            adapter = groupAdapter
        }
    }

    @SuppressLint("InflateParams")
    private fun initCategoryDialog() {
        val binding: DialogCategoryDetailBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.dialog_category_detail,
            this.view as ViewGroup,
            false
        )
        binding.fragment = this
        val categoryDialog = AlertDialog.Builder(context ?: return)
            .setView(binding.root)
            .setPositiveButton(getString(R.string.save), null)
            .setNegativeButton(getString(R.string.cancel)) { _, _ -> }
            .create()
        categoryDialog.setOnShowListener {
            categoryDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                val value = category.value ?: return@setOnClickListener
                val error = value.second.name.isValidName()
                if (error == 0) {
                    budgetViewModel.upsert(value)
                    categoryDialog.hide()
                } else {
                    categoryNameEditText.error = getString(error)
                }
            }
        }
        categoryDialog.show()
    }

    fun categoryNameValidator(name: String) = name.isValidName()

    fun showSearchResultRecyclerView() {
        searchResultRecyclerView.visibility = View.VISIBLE
        membersLabel.visibility = View.INVISIBLE
        membersRecyclerView.visibility = View.INVISIBLE
    }

    fun hideSearchResultRecyclerView() {
        searchView.onActionViewCollapsed()
        searchResultRecyclerView.visibility = View.INVISIBLE
        membersLabel.visibility = View.VISIBLE
        membersRecyclerView.visibility = View.VISIBLE
    }

}