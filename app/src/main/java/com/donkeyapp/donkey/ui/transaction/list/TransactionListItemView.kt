package com.donkeyapp.donkey.ui.transaction.list

import android.graphics.Color
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemTransactionListBinding
import com.donkeyapp.donkey.databinding.ItemTransactionListHeaderBinding
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Transaction
import com.donkeyapp.donkey.ui.transaction.detail.Direction
import com.xwray.groupie.databinding.BindableItem
import org.jetbrains.anko.textColor
import org.threeten.bp.LocalDate

class TransactionListItemView(
    val direction: Direction,
    val transaction: Entity<Transaction>
) : BindableItem<ItemTransactionListBinding>() {

    override fun getLayout() = R.layout.item_transaction_list

    override fun bind(binding: ItemTransactionListBinding, pos: Int) {
        binding.transaction = transaction.data
        binding.amount.textColor = if (direction == Direction.INCOME) {
            Color.GREEN
        } else {
            Color.RED
        }

    }

}

class TransactionListItemHeaderView(
    private val localDate: LocalDate
) : BindableItem<ItemTransactionListHeaderBinding>() {

    override fun getLayout() = R.layout.item_transaction_list_header

    override fun bind(binding: ItemTransactionListHeaderBinding, pos: Int) {
        binding.day = localDate.dayOfMonth
    }

}