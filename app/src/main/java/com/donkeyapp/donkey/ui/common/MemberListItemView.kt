package com.donkeyapp.donkey.ui.common

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemMemberDetailBinding
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.model.User
import com.xwray.groupie.databinding.BindableItem

class MemberListItemView(
    private val role: MutableMap.MutableEntry<String, Int>,
    private val user: Entity<User>,
    private val currentUserRole: Role,
    private val onDelete: (String) -> Unit
) : BindableItem<ItemMemberDetailBinding>() {

    override fun getLayout() = R.layout.item_member_detail

    override fun bind(binding: ItemMemberDetailBinding, position: Int) {
        binding.currentUserId = currentUserId()
        binding.role = role
        binding.user = user.data
        binding.hasEditPermission = currentUserRole >= Role.ADMIN
        binding.deleteMemberBtn.setOnClickListener {
            onDelete(user.id)
        }
    }

}