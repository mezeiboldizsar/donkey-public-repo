package com.donkeyapp.donkey.ui.account.detail

import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import androidx.navigation.Navigation
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.FragmentAccountDetailBinding
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.ui.common.MemberViewModel
import com.donkeyapp.donkey.ui.common.RecyclerViewWrapper
import com.donkeyapp.donkey.ui.common.UserSearchViewModel
import com.donkeyapp.donkey.utils.Saved
import kotlinx.android.synthetic.main.fragment_account_detail.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class AccountDetailFragment :
    BindableScopedFragment<FragmentAccountDetailBinding>(R.layout.fragment_account_detail) {

    private val accountIdArg by lazy {
        val args = arguments?.let { AccountDetailFragmentArgs.fromBundle(it) }
        args?.accountId
    }

    private val accountId by lazy {
        accountIdArg
    }

    val accountViewModel by inject<AccountDetailViewModel> { parametersOf(accountId) }

    val memberViewModel by inject<MemberViewModel> {
        parametersOf(accountViewModel.account.map {
            it.data.roles
        })
    }

    val searchViewModel by inject<UserSearchViewModel> {
        parametersOf({ userId: String -> addUser(userId) })
    }

    override fun bindUI() {
        RecyclerViewWrapper(membersRecyclerView, this)
        RecyclerViewWrapper(searchResultRecyclerView, this)
        binding.fragment = this
        binding.isEditMode = accountIdArg != null
        binding.lifecycleOwner = this

        accountViewModel.state.observe(this, Observer {
            if (it == Saved) {
                close()
            }
        })
    }

    fun showSearchResultRecyclerView() {
        searchResultRecyclerView.visibility = View.VISIBLE
        membersLabel.visibility = View.INVISIBLE
        membersRecyclerView.visibility = View.INVISIBLE
    }

    fun hideSearchResultRecyclerView() {
        searchView.onActionViewCollapsed()
        searchResultRecyclerView.visibility = View.INVISIBLE
        membersLabel.visibility = View.VISIBLE
        membersRecyclerView.visibility = View.VISIBLE
    }

    private fun close() {
        Navigation.findNavController(view ?: return).popBackStack()
    }

    private fun addUser(userId: String) {
        memberViewModel.add(userId)
        hideSearchResultRecyclerView()
    }

}