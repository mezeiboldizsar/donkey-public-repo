package com.donkeyapp.donkey.ui.budget.detail

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.utils.isLetterSpaceOrNumber

fun String.isValidName(): Int {
    val name = this.trim()
    if (name.length < 3 || name.length > 20) return R.string.name_between
    if (!name.fold(true) { sum, char -> sum && char.isLetterSpaceOrNumber() }) return R.string.not_alpha
    return 0
}
