package com.donkeyapp.donkey.ui.account.list

import androidx.lifecycle.*
import com.donkeyapp.donkey.store.firebase.model.ownerIds
import com.donkeyapp.donkey.store.firebase.repository.FirestoreAccountRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreExpenseRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreIncomeRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreUserRepository

abstract class AccountListViewModel : ViewModel() {
    abstract val accountListItems: LiveData<List<AccountListItemView>>
}

class AccountListViewModelImpl(
    accountRepository: FirestoreAccountRepository,
    private val userRepository: FirestoreUserRepository,
    private val incomeRepository: FirestoreIncomeRepository,
    private val expenseRepository: FirestoreExpenseRepository
) : AccountListViewModel() {

    private val accounts = accountRepository.findAll().switchMap { accounts ->
        liveData {
            emit(accounts.map {
                it.data.money.amount += (incomeRepository.sum(it.id) - expenseRepository.sum(it.id))
                it
            })
        }
    }

    private val users = accounts.switchMap { accounts ->
        userRepository.findMany(accounts.map { it.data }.ownerIds())
    }

    override val accountListItems = accounts.switchMap { budgets ->
        users.map { users ->
            val userMap = users.groupBy { it.id }
            budgets.map {
                AccountListItemView(it.id, it.data, userMap[it.data.ownerId()]?.firstOrNull()?.data)
            }
        }
    }

}

