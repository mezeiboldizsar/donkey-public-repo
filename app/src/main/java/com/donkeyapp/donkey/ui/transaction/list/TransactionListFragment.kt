package com.donkeyapp.donkey.ui.transaction.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.FragmentTransactionListBinding
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.ui.common.RecyclerViewWrapper
import com.donkeyapp.donkey.ui.transaction.detail.Direction
import kotlinx.android.synthetic.main.fragment_transaction_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class TransactionListFragment :
    BindableScopedFragment<FragmentTransactionListBinding>(
        R.layout.fragment_transaction_list
    ) {

    private val viewModel by viewModel<TransactionListViewModel>()

    override fun bindUI() {
        initTransactionList()
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addBtn.setOnClickListener {
            openTransactionDetailFragment(null, Direction.EXPENSE)
        }
        RecyclerViewWrapper(sumRecyclerView, this)
    }

    private fun initTransactionList() {
        val recyclerView = RecyclerViewWrapper(transactionRV, this)
        recyclerView.groupAdapter.setOnItemLongClickListener { item, _ ->
            (item as? TransactionListItemView)?.let {
                openTransactionDetailFragment(it.transaction.id, it.direction)
            }
            true
        }
    }

    private fun openTransactionDetailFragment(transactionId: String?, direction: Direction) {
        val actionDetail = TransactionListFragmentDirections
            .actionToTransactionDetailFragment(transactionId, direction)
        navigate(actionDetail)
    }

}
