package com.donkeyapp.donkey.ui.transaction.list

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.*
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Money
import com.donkeyapp.donkey.store.firebase.model.Transaction
import com.donkeyapp.donkey.store.firebase.repository.FirestoreExpenseRepository
import com.donkeyapp.donkey.store.firebase.repository.FirestoreIncomeRepository
import com.donkeyapp.donkey.ui.transaction.detail.Direction
import com.donkeyapp.donkey.utils.concat
import com.donkeyapp.donkey.utils.toLocalDate
import com.xwray.groupie.databinding.BindableItem
import org.threeten.bp.LocalDate

abstract class TransactionListViewModel : ViewModel() {
    abstract val transactionListItems: LiveData<List<BindableItem<out ViewDataBinding>>>
    abstract val date: LiveData<LocalDate>

    abstract val sumListItems: LiveData<List<SumItemView>>

    abstract fun addMonth(monthsToAdd: Long)
}

class TransactionListViewModelImpl(
    private val incomeRepository: FirestoreIncomeRepository,
    private val expenseRepository: FirestoreExpenseRepository
) : TransactionListViewModel() {

    override val date = MutableLiveData<LocalDate>().apply {
        postValue(LocalDate.now())
    }

    private val incomes = date.switchMap {
        incomeRepository.findAll(it.year, it.monthValue)
    }

    private val expenses = date.switchMap {
        expenseRepository.findAll(it.year, it.monthValue)
    }

    private val incomeSum = incomes.map { it.sum() }

    private val expenseSum = expenses.map { it.sum() }

    override val sumListItems = incomeSum.union(expenseSum) { currency, income, expense ->
        SumItemView(Money(income ?: 0.0, currency), Money(expense ?: 0.0, currency))
    }.map { it.values.toList() }

    private val incomeListViews = incomes.map { transactions ->
        transactions.sortedByDescending { it.data.date }.map {
            TransactionListItemView(Direction.INCOME, it)
        }
    }

    private val expenseListViews = expenses.map { transactions ->
        transactions.sortedByDescending { it.data.date }.map {
            TransactionListItemView(Direction.EXPENSE, it)
        }
    }

    override val transactionListItems = incomeListViews.concat(expenseListViews).map { items ->
        items.groupBy { it.transaction.data.date.toLocalDate() }
            .toSortedMap(reverseOrder())
            .flatMap { entry ->
                listOf(TransactionListItemHeaderView(entry.key)) +
                        entry.value.sortedByDescending { it.transaction.data.date }
            }
    }

    override fun addMonth(monthsToAdd: Long) {
        val currentDate = date.value ?: return
        date.postValue(currentDate.plusMonths(monthsToAdd))
    }

}

private fun List<Entity<Transaction>>.sum(): Map<String, Double> =
    groupBy { it.data.money.currency }.mapValues { value ->
        value.value.sumByDouble { it.data.money.amount }
    }.toSortedMap()

private fun <A : Comparable<A>, B, C> LiveData<Map<A, B>>.union(
    with: LiveData<Map<A, B>>,
    unionF: (A, B?, B?) -> C
) = MediatorLiveData<Map<A, C>>().apply {

    var first: Map<A, B>? = null
    var second: Map<A, B>? = null

    fun update() {
        if (first != null && second != null) {
            postValue(
                (first!!.keys + second!!.keys).map {
                    it to unionF(it, first!![it], second!![it])
                }.toMap().toSortedMap()
            )

        }
    }

    addSource(this@union) {
        first = it
        update()
    }

    addSource(with) {
        second = it
        update()
    }

}