package com.donkeyapp.donkey.ui.common

import androidx.lifecycle.*
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.repository.FirestoreUserRepository

abstract class MemberViewModel : ViewModel() {
    abstract val members: LiveData<List<MemberListItemView>>
    abstract val hasEditPermission: LiveData<Boolean>

    abstract fun add(userId: String)
    abstract fun delete(userId: String)
}

class MemberViewModelImpl(
    private val roles: MutableLiveData<MutableMap<String, Int>>,
    private val userRepository: FirestoreUserRepository
) : MemberViewModel() {

    override val hasEditPermission = MutableLiveData<Boolean>().apply {
        value = true
    }

    override val members = roles.switchMap { roles ->
        hasEditPermission.postValue(roles[currentUserId()] ?: 0 >= Role.ADMIN.ordinal)
        userRepository.findMany(roles.keys.filter { it != currentUserId() }).map { users ->
            users.mapNotNull { user ->
                MemberListItemView(
                    roles.entries.firstOrNull { it.key == user.id } ?: return@mapNotNull null,
                    user,
                    Role.get(roles[currentUserId()] ?: return@mapNotNull null)
                ) { userId -> delete(userId) }
            }
        }
    }

    override fun add(userId: String) {
        val value = roles.value ?: return
        value[userId] = 1
        roles.postValue(value)
    }

    override fun delete(userId: String) {
        val value = roles.value ?: return
        value.remove(userId)
        roles.postValue(value)
    }

}