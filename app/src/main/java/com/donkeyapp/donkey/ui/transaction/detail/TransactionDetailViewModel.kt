package com.donkeyapp.donkey.ui.transaction.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.donkeyapp.donkey.store.firebase.model.*
import com.donkeyapp.donkey.store.firebase.repository.*
import com.donkeyapp.donkey.ui.common.BaseViewModel
import com.donkeyapp.donkey.utils.*
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime

enum class Direction {
    INCOME, EXPENSE
}

abstract class TransactionDetailViewModel : BaseViewModel() {
    abstract val currentDirection: LiveData<Direction>
    abstract val transaction: LiveData<Entity<Transaction>>
    abstract val accountSpinnerItems: LiveData<List<AccountSpinnerItem>>
    abstract val budgetSpinnerItems: LiveData<List<BudgetSpinnerItem>>
    abstract val categorySpinnerItems: LiveData<List<CategorySpinnerItem>>

    abstract fun setDirection(direction: Direction)
    abstract fun setDate(date: LocalDate?, time: LocalTime?)

    abstract fun setAccount(item: AccountSpinnerItem)
    abstract fun setBudget(item: BudgetSpinnerItem)
    abstract fun setCategory(item: CategorySpinnerItem)

    abstract fun upsert()
    abstract fun delete()
}

class TransactionDetailViewModelImpl(
    private val transactionId: String?,
    private val direction: Direction,
    private val incomeRepository: FirestoreIncomeRepository,
    private val expenseRepository: FirestoreExpenseRepository,
    accountRepository: FirestoreAccountRepository,
    budgetRepository: FirestoreBudgetRepository
) : TransactionDetailViewModel() {

    override val currentDirection = MutableLiveData<Direction>().apply {
        value = direction
    }

    private val newTransaction by lazy {
        Entity(data = Transaction())
    }

    override val transaction = liveData {
        if (transactionId == null) {
            state.postValue(Unsavable)
            emit(newTransaction)
        } else {
            emitSource(
                getRepository(direction).findOne(transactionId).map {
                    state.postValue(SaveState.isSavable(it != null))
                    it ?: newTransaction
                }
            )
        }
    }.toMutable()

    private val accounts = accountRepository.findAll()

    override val accountSpinnerItems = accounts.map { accounts ->
        listOf(AccountSpinnerItem()) + accounts.map { it.data.toSpinnerItem(it.id) }
    }

    private val budgets = budgetRepository.findAll()

    override val budgetSpinnerItems = budgets.map { budgets ->
        listOf(BudgetSpinnerItem()) + budgets.map { it.data.toSpinnerItem(it.id) }
    }

    override val categorySpinnerItems = transaction.map { transaction ->
        val budgets = budgets.value
        val budget =
            budgets?.firstOrNull { it.id == transaction.data.budget?.id } ?: return@map listOf(
                CategorySpinnerItem()
            )
        listOf(CategorySpinnerItem()) + budget.data.categories.map {
            CategorySpinnerItem(it.key, it.value.name, true)
        }
    }

    override fun setDirection(direction: Direction) {
        currentDirection.postValue(direction)
    }

    override fun setDate(date: LocalDate?, time: LocalTime?) {
        val value = transaction.value ?: return
        val localDate = date ?: value.data.date.toLocalDate()
        val localTime = time ?: value.data.date.toLocalTime()
        val inst = localDate.toInstant(localTime)
        val newDate = inst.toTimestamp()
        value.data.date = newDate
        value.data.year = localDate.year
        value.data.month = localDate.monthValue
        transaction.postValue(value)
    }

    override fun setAccount(item: AccountSpinnerItem) {
        val value = transaction.value ?: return
        value.data.account = TransactionAccount(
            item.id ?: return,
            item.text
        )
        value.data.money.currency = item.currency
        transaction.postValue(value)
    }

    override fun setBudget(item: BudgetSpinnerItem) {
        val value = transaction.value ?: return
        value.data.budget = TransactionBudget(item.id, item.text)
        transaction.postValue(value)
    }

    override fun setCategory(item: CategorySpinnerItem) {
        val value = transaction.value ?: return
        value.data.budget?.category = TransactionBudgetCategory(item.id, item.text)
    }

    override fun upsert() {
        val value = transaction.value ?: return
        state.postValue(Saving)

        val budgetRoles = budgets.value?.firstOrNull {
            it.id == value.data.budget?.id
        }?.data?.roles ?: mutableMapOf()

        val accountRoles = accounts.value?.firstOrNull {
            it.id == value.data.account?.id
        }?.data?.roles ?: mutableMapOf()

        value.data.roles = (budgetRoles.keys + accountRoles.keys).map {
            it to (budgetRoles[it]?.times(10) ?: 0) + (accountRoles[it] ?: 0)
        }.toMap()

        value.data.userIds = value.data.roles.keys.toList()

        if (value.id == "") {
            getRepository(direction).create(value.data)
        } else {
            val newDirection = currentDirection.value ?: direction
            if (newDirection != direction) {
                getRepository(direction).delete(value.id)
                getRepository(newDirection).create(value.data)
            } else {
                getRepository(newDirection).update(value)
            }
        }

        state.postValue(Saved)
    }

    override fun delete() {
        state.postValue(Saving)
        getRepository(direction).delete(transactionId ?: return)
        state.postValue(Saved)
    }

    private fun getRepository(direction: Direction): FirestoreTransactionRepository =
        if (direction == Direction.INCOME) incomeRepository else expenseRepository

}
