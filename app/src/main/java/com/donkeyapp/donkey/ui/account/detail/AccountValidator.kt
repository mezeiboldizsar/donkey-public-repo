package com.donkeyapp.donkey.ui.account.detail

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.store.firebase.model.Account
import com.donkeyapp.donkey.utils.isLetterSpaceOrNumber
import com.donkeyapp.donkey.utils.toDoubleOrZero
import java.lang.Exception

interface Validator<T> {
    fun validate(data: T?): Boolean
}

class AccountValidator : Validator<Account> {

    override fun validate(data: Account?): Boolean {
        return data != null &&
                data.name.isAccountName() == 0 &&
                data.money.amount.toString().isValidAmount() == 0
    }

}

fun String.isAccountName(): Int {
    if (length < 3 || length > 20) return R.string.name_between
    if (!fold(true) { sum, char ->
            sum && char.isLetterSpaceOrNumber()
        }) return R.string.not_alpha
    return 0
}

fun String.isValidAmount(): Int {
    return try {
        val amount = toDoubleOrZero()
        if (amount == 0.0) {
            R.string.amount_value_is_zero
        } else {
            0
        }
    } catch (ex: Exception) {
        R.string.invalid_amount
    }
}