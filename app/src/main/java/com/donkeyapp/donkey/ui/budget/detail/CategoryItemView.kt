package com.donkeyapp.donkey.ui.budget.detail

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemBudgetDetailCategoryBinding
import com.donkeyapp.donkey.store.firebase.model.Category
import com.xwray.groupie.databinding.BindableItem

class CategoryItemView(
    val category: Pair<String, Category>,
    private val onDelete: (String) -> Unit
) : BindableItem<ItemBudgetDetailCategoryBinding>() {
    override fun getLayout() = R.layout.item_budget_detail_category

    override fun bind(binding: ItemBudgetDetailCategoryBinding, pos: Int) {
        binding.categoryName = category.second.name
        binding.deleteBtn.setOnClickListener {
            onDelete(category.first)
        }
    }

}