package com.donkeyapp.donkey.ui.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.donkeyapp.donkey.utils.SaveState
import com.donkeyapp.donkey.utils.Unsavable

abstract class BaseViewModel : ViewModel() {
    val state = MutableLiveData<SaveState>().apply {
        postValue(Unsavable)
    }
    val error = MutableLiveData<String>()
}