package com.donkeyapp.donkey.ui.account.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.FragmentAccountListBinding
import com.donkeyapp.donkey.ui.common.BindableScopedFragment
import com.donkeyapp.donkey.ui.common.RecyclerViewWrapper
import kotlinx.android.synthetic.main.fragment_account_list.*
import org.koin.android.ext.android.inject

class AccountListFragment :
    BindableScopedFragment<FragmentAccountListBinding>(R.layout.fragment_account_list) {

    val viewModel by inject<AccountListViewModel>()

    override fun bindUI() {
        initAccountList()
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        addBtn.setOnClickListener {
            openAccountDetailFragment(null)
        }
    }

    private fun openAccountDetailFragment(accountId: String?) {
        val actionDetail = AccountListFragmentDirections.toAccountDetailFragment(accountId)
        navigate(actionDetail)
    }

    private fun initAccountList() {
        val recyclerView = RecyclerViewWrapper(accountListRW, this)
        recyclerView.groupAdapter.setOnItemLongClickListener { item, _ ->
            (item as? AccountListItemView)?.let {
                openAccountDetailFragment(it.id)
            }
            true
        }
    }

}
