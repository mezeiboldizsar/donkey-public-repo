package com.donkeyapp.donkey.ui.transaction.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemSumListBinding
import com.donkeyapp.donkey.store.firebase.model.Money
import com.xwray.groupie.databinding.BindableItem

class SumItemView(
    private val incomeSum: Money,
    private val expenseSum: Money
) : BindableItem<ItemSumListBinding>() {
    override fun getLayout() = R.layout.item_sum_list

    override fun bind(binding: ItemSumListBinding, pos: Int) {
        binding.incomeSum = incomeSum
        binding.expenseSum = expenseSum
        binding.totalSum = Money(incomeSum.amount - expenseSum.amount, incomeSum.currency)
    }

}