package com.donkeyapp.donkey.ui.budget.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Budget
import com.donkeyapp.donkey.store.firebase.model.Category
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.repository.FirestoreBudgetRepository
import com.donkeyapp.donkey.ui.account.detail.Validator
import com.donkeyapp.donkey.ui.common.BaseViewModel
import com.donkeyapp.donkey.utils.SaveState
import com.donkeyapp.donkey.utils.Saved
import com.donkeyapp.donkey.utils.Saving
import com.donkeyapp.donkey.utils.toMutable

abstract class BudgetDetailViewModel : BaseViewModel() {
    protected abstract val validator: Validator<Budget>
    abstract val budget: LiveData<Entity<Budget>>
    abstract val categoryItemViews: LiveData<List<CategoryItemView>>

    abstract fun upsert()
    abstract fun upsert(category: Pair<String, Category>)
    abstract fun delete()

    fun validateName(name: String): Int = name.isValidBudgetName().also {
        state.postValue(SaveState.isSavable(validator.validate(budget.value?.data)))
    }

}

class BudgetDetailViewModelImpl(
    private val budgetId: String?,
    override val validator: Validator<Budget>,
    private val budgetRepository: FirestoreBudgetRepository
) : BudgetDetailViewModel() {

    private val newBudget by lazy {
        Entity(data = Budget(roles = mutableMapOf(currentUserId() to Role.OWNER.ordinal)))
    }

    override val budget = liveData {
        if (budgetId == null) {
            emit(newBudget)
        } else {
            emitSource(budgetRepository.findOne(budgetId).map {
                it ?: newBudget
            })
        }
    }.toMutable()

    override val categoryItemViews = budget.map { budget ->
        budget.data.categories.map { c ->
            CategoryItemView(c.toPair()) {
                deleteCategory(it)
            }
        }
    }

    override fun upsert() {
        val value = budget.value ?: return
        state.postValue(Saving)
        value.data.userIds = value.data.roles.keys.toList()
        if (budgetId == null) {
            budgetRepository.create(value.data)
        } else {
            budgetRepository.update(value)
        }
        state.postValue(Saved)
    }

    override fun upsert(category: Pair<String, Category>) {
        val value = budget.value ?: return
        value.data.categories[category.first] = category.second
        budget.postValue(value)
    }

    override fun delete() {
        state.postValue(Saving)
        if (budgetId != null) {
            budgetRepository.delete(budgetId)
        }
        state.postValue(Saved)
    }

    private fun deleteCategory(categoryId: String) {
        val value = budget.value ?: return
        value.data.categories.remove(categoryId)
        budget.postValue(value)
    }

}