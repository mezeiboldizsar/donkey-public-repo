package com.donkeyapp.donkey.ui.budget.detail

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.store.firebase.model.Budget
import com.donkeyapp.donkey.ui.account.detail.Validator
import com.donkeyapp.donkey.utils.isLetterSpaceOrNumber

class BudgetValidator : Validator<Budget> {

    override fun validate(data: Budget?): Boolean =
        data != null && data.name.isValidBudgetName() == 0

}

fun String.isValidBudgetName(): Int {
    if (length < 3 || length > 20) return R.string.name_between
    if (!fold(true) { sum, char ->
            sum && char.isLetterSpaceOrNumber()
        }) return R.string.not_alpha
    return 0
}