package com.donkeyapp.donkey.ui.budget.list

import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.databinding.ItemBudgetListBinding
import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Budget
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.store.firebase.model.User
import com.donkeyapp.donkey.utils.getString
import com.xwray.groupie.databinding.BindableItem

class BudgetListItemView(
    val id: String,
    private val budget: Budget,
    private val owner: User?
) : BindableItem<ItemBudgetListBinding>() {

    override fun getLayout() = R.layout.item_budget_list

    override fun bind(binding: ItemBudgetListBinding, pos: Int) {
        binding.budget = budget
        binding.owner = owner
        binding.roleTextView.text =
            binding.roleTextView.context.getString(Role.get(budget.role(currentUserId())).toString())
    }

}