package com.donkeyapp.donkey.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.store.firebase.googleSignInClient
import com.donkeyapp.donkey.store.firebase.repository.FirestoreUserRepository
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.koin.android.ext.android.inject


class SignInActivity : AppCompatActivity() {

    private val userRepository by inject<FirestoreUserRepository>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        loginBtn.setOnClickListener {
            val signInIntent = googleSignInClient(this).signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
            FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener {
                if (it.isSuccessful) {
                    val dialog = indeterminateProgressDialog("Init current user")
                    userRepository.initCurrentUserIfFirstTime {
                        startActivity(intentFor<MainActivity>().newTask().clearTask())
                        dialog.dismiss()
                    }
                } else {
                    layout.longSnackbar(it.exception?.message ?: "")
                }
            }
        } catch (e: ApiException) {
            layout.longSnackbar(e.message ?: "")
        }
    }

    companion object {
        private const val RC_SIGN_IN = 1
    }

}