package com.donkeyapp.donkey.ui.transaction.detail

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import org.threeten.bp.*

fun Context.datePicker(date: LocalDate?, listener: (LocalDate) -> Unit) {
    val current = date ?: LocalDate.now()
    val onDateSelectedListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        val selected = LocalDate.of(year, month + 1, dayOfMonth)
        listener.invoke(selected)
    }
    val dialog = DatePickerDialog(
        this,
        onDateSelectedListener,
        current.year,
        current.monthValue - 1,
        current.dayOfMonth
    )
    dialog.show()
}

fun Context.timePicker(time: LocalTime?, listener: (LocalTime) -> Unit) {
    val currentTime = time ?: LocalTime.now()
    val onTimeSelectedListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        val selected = LocalTime.of(hourOfDay, minute)
        listener.invoke(selected)
    }
    val dialog = TimePickerDialog(
        this,
        onTimeSelectedListener,
        currentTime.hour,
        currentTime.minute,
        true
    )
    dialog.show()
}

