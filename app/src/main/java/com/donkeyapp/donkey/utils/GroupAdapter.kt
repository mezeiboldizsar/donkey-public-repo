package com.donkeyapp.donkey.utils

import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter

fun GroupAdapter<*>.replace(items: List<Group>) {
    this.clear()
    this.addAll(items)
}