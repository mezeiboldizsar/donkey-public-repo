package com.donkeyapp.donkey.utils

import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.databinding.InverseBindingListener
import com.donkeyapp.donkey.bindings.SpinnerItemRule

fun <T> Spinner.setEntries(entries: List<SpinnerItem<T>>) {
    val arrayAdapter =
        object :
            ArrayAdapter<SpinnerItem<T>>(context, android.R.layout.simple_spinner_item, entries) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                return if (entries[position].visible) super.getDropDownView(
                    position,
                    null,
                    parent
                )
                else TextView(context).apply {
                    height = 0
                    visibility = View.GONE
                }
            }
        }
    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    adapter = arrayAdapter
}

fun <T> Spinner.setSelectedValue(entries: List<SpinnerItem<T>>, id: T) {
    if (adapter != null) {
        val pos = entries.indexOfFirst { it.id == id }
        setSelection(pos, false)
        tag = pos
    }
}

fun Spinner.setSpinnerInverseBindingListener(
    listener: InverseBindingListener?,
    spinnerRule: SpinnerItemRule?
) {
    onItemSelectedListener =
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (tag != position) {
                    listener?.onChange()
                    spinnerRule?.onSelectedValueChanged(selectedItem as? SpinnerItem<Any>)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
}

fun Spinner.getSpinnerItemId(): Any? {
    return (selectedItem as? SpinnerItem<*>)?.id
}