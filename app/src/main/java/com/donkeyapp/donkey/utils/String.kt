package com.donkeyapp.donkey.utils

fun String.toDoubleOrZero(): Double = replace(",", "").toDoubleOrNull() ?: 0.0