package com.donkeyapp.donkey.utils.firebase

import android.util.Log
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

fun <T> Task<QuerySnapshot>.onSimpleQueryCompleteListener(
    clazz: Class<T>, f: (List<Entity<T>>) -> Unit
) {
    addOnSuccessListener {
        f(it.toObjectsWithId(clazz))
    }
    addOnFailureListener {
        Log.e("FIRESTORE", "Task failure", it)
        f(listOf())
    }
}

suspend fun <T> Task<QuerySnapshot>.await(clazz: Class<T>): List<Entity<T>> =
    withContext(Dispatchers.IO) {
        val latch = CountDownLatch(1)
        val doc: MutableList<Entity<T>> = mutableListOf()
        addOnSuccessListener {
            doc.addAll(it.toObjectsWithId(clazz))
            latch.countDown()
        }
        addOnFailureListener {
            Log.e("FIRESTORE", "Task failure", it)
            latch.countDown()
        }
        latch.await(10, TimeUnit.SECONDS)
        doc
    }
