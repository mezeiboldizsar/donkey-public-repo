package com.donkeyapp.donkey.utils.firebase

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*

abstract class FirebaseLiveData<T> : LiveData<T>() {

    protected var registration: ListenerRegistration? = null

    override fun onInactive() {
        super.onInactive()
        registration?.remove()
    }

}

class FirebaseDocumentLiveData<T>(
    private val documentReference: DocumentReference,
    private val converter: (DocumentSnapshot) -> T?
) : FirebaseLiveData<T?>() {

    override fun onActive() {
        super.onActive()
        registration =
            documentReference.addSnapshotListener { snapshot, exception ->
                postValue(
                    if (snapshot != null) {
                        converter(snapshot)
                    } else {
                        Log.e("FIRESTORE", "FirebaseLiveData failure", exception)
                        null
                    }
                )
            }
    }

}

class FirebaseQueryLiveData<T>(
    private val query: Query,
    private val converter: (QuerySnapshot) -> List<T>
) : FirebaseLiveData<List<T>>() {

    override fun onActive() {
        super.onActive()
        registration = query.addSnapshotListener { snapshot, exception ->
            postValue(
                if (snapshot != null) {
                    converter(snapshot)
                } else {
                    Log.e("FIRESTORE", "FirebaseLiveData failure", exception)
                    listOf()
                }
            )
        }
    }

}
