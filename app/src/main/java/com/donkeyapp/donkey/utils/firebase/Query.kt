package com.donkeyapp.donkey.utils.firebase

import androidx.lifecycle.LiveData
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot

fun <T> Query.toLiveData(clazz: Class<T>): LiveData<List<Entity<T>>> =
    FirebaseQueryLiveData(this) {
        it.toObjectsWithId(clazz)
    }

fun <T> QuerySnapshot.toObjectsWithId(clazz: Class<T>): List<Entity<T>> =
    documents.mapNotNull {
        it.toObjectWithId(clazz)
    }

fun <T> DocumentSnapshot.toObjectWithId(clazz: Class<T>): Entity<T>? {
    return Entity(id, this.toObject(clazz) ?: return null)
}
