package com.donkeyapp.donkey.utils.firebase

import androidx.lifecycle.LiveData
import com.donkeyapp.donkey.store.firebase.model.Entity
import com.google.firebase.firestore.DocumentReference

fun <T> DocumentReference.toLiveData(clazz: Class<T>): LiveData<Entity<T>?> =
    FirebaseDocumentLiveData(this) { it.toObjectWithId(clazz) }