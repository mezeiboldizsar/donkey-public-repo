package com.donkeyapp.donkey.utils

import androidx.lifecycle.*

fun <T> LiveData<T>.toMutable(): MutableLiveData<T> {
    return MediatorLiveData<T>().apply {
        addSource(this@toMutable) {
            postValue(it)
        }
    }
}

fun <T> LiveData<List<T>>.concat(liveData: LiveData<List<T>>) = MediatorLiveData<List<T>>().apply {
    var first: List<T>? = null
    var second: List<T>? = null

    fun update() {
        if (first != null && second != null) {
            postValue(first!! + second!!)
        }
    }

    addSource(this@concat) {
        first = it
        update()
    }

    addSource(liveData) {
        second = it
        update()
    }

}