package com.donkeyapp.donkey.utils

import com.donkeyapp.donkey.store.firebase.currentUserId
import com.donkeyapp.donkey.store.firebase.model.Account
import com.donkeyapp.donkey.store.firebase.model.Budget
import com.donkeyapp.donkey.store.firebase.model.Role

abstract class SpinnerItem<T> {
    abstract val id: T?
    abstract val text: String
    abstract val visible: Boolean
    override fun toString(): String = text
}

class RoleSpinnerItem(
    override val id: Int,
    override val text: String = "",
    override val visible: Boolean = false
) : SpinnerItem<Int>()

class AccountSpinnerItem(
    val currency: String = "",
    override val id: String? = null,
    override val text: String = "Select an account",
    override val visible: Boolean = false
) : SpinnerItem<String>()

class BudgetSpinnerItem(
    override val id: String = "",
    override val text: String = "Select a budget",
    override val visible: Boolean = false
) : SpinnerItem<String>()

class CategorySpinnerItem(
    override val id: String = "",
    override val text: String = "Select a category",
    override val visible: Boolean = false
) : SpinnerItem<String>()

class CurrencySpinnerItem(
    override val id: String = "",
    override val text: String = "",
    override val visible: Boolean = false
) : SpinnerItem<String>()

fun Account.toSpinnerItem(accountId: String) = AccountSpinnerItem(
    money.currency,
    accountId,
    name,
    (roles[currentUserId()] ?: 0) >= Role.ADMIN.ordinal
)

fun Budget.toSpinnerItem(budgetId: String) = BudgetSpinnerItem(
    budgetId,
    name,
    (roles[currentUserId()] ?: 0) >= Role.ADMIN.ordinal
)