package com.donkeyapp.donkey.utils

import android.content.Context

fun Context.getString(resourceIdName: String): String? =
    getString(resources.getIdentifier(resourceIdName, "string", packageName))
