package com.donkeyapp.donkey.utils

import com.google.firebase.Timestamp
import org.threeten.bp.*

fun Instant.toLocalDate(): LocalDate {
    return LocalDateTime.ofInstant(this, OffsetDateTime.now().offset).toLocalDate()
}

fun Instant.toLocalTime(): LocalTime {
    return LocalDateTime.ofInstant(this, OffsetDateTime.now().offset).toLocalTime()
}

fun Instant.toTimestamp(): Timestamp = Timestamp(epochSecond, nano)

fun LocalDate.toInstant(time: LocalTime) =
    LocalDateTime.of(this, time).toInstant(OffsetDateTime.now().offset)

fun Timestamp.toLocalDate(): LocalDate =
    Instant.ofEpochSecond(seconds).toLocalDate()

fun Timestamp.toLocalTime(): LocalTime =
    Instant.ofEpochSecond(seconds).toLocalTime()
