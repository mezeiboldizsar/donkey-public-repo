package com.donkeyapp.donkey.utils

import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

abstract class SwipeListener : View.OnTouchListener {

    private var x1: Float = 0f
    private var x2: Float = 0f

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> x1 = event.x
            MotionEvent.ACTION_UP -> {
                x2 = event.x
                val deltaX = x2 - x1
                if (abs(deltaX) >= MIN_DISTANCE) {
                    if (deltaX > 0) {
                        onRightSwipe()
                    } else {
                        onLeftSwipe()
                    }
                }
            }
        }
        return true
    }

    abstract fun onRightSwipe()

    abstract fun onLeftSwipe()

    companion object {
        private const val MIN_DISTANCE = 150
    }

}