package com.donkeyapp.donkey.utils

fun Char.isLetterSpaceOrNumber() = this.isLetter() || this.isWhitespace() || this.isDigit()