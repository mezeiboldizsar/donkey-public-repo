package com.donkeyapp.donkey.utils

sealed class SaveState(val isSavable: Boolean = false) {
    companion object {
        fun isSavable(isSavable: Boolean) = if (isSavable) Savable else Unsavable
    }
}

object Unsavable : SaveState()
object Savable : SaveState(true)
object Saving : SaveState()
object Saved : SaveState(true)
class Error(val msg: String) : SaveState()