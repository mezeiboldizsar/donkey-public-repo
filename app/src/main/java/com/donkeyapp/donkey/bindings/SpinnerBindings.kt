package com.donkeyapp.donkey.bindings

import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.donkeyapp.donkey.store.firebase.model.Role
import com.donkeyapp.donkey.utils.*

class SpinnerBindings {

    @BindingAdapter(value = ["entries", "selectedValue"], requireAll = true)
    fun <T> Spinner.setEntries(entries: List<SpinnerItem<T>>?, selectedValueId: T?) {
        if (entries != null) {
            setEntries(entries)
            if (selectedValueId != null) {
                setSelectedValue(entries, selectedValueId)
            }
        }
    }

    @BindingAdapter("selectedValue")
    fun Spinner.setEntries(selectedValueId: Int?) {
        if (selectedValueId != null) {
            val roles = Role.values().map {
                RoleSpinnerItem(
                    it.ordinal,
                    context.getString(it.toString()) ?: "",
                    it != Role.NONE && it != Role.OWNER
                )
            }
            setEntries(roles)
            setSelectedValue(roles, selectedValueId)
        }
    }

    @BindingAdapter(value = ["isCurrency", "selectedValue"], requireAll = true)
    fun Spinner.setEntries(isCurrency: Boolean?, selectedValueId: String?) {
        if (isCurrency == true) {
            setEntries(currencySpinnerItems)
            if (selectedValueId != null) {
                setSelectedValue(currencySpinnerItems, selectedValueId)
            }
        }
    }

    @BindingAdapter(value = ["selectedValueAttrChanged", "extraListener"], requireAll = false)
    fun Spinner.setInverseBindingListener(
        inverseBindingListener: InverseBindingListener?,
        extraListener: SpinnerItemRule?
    ) {
        if (inverseBindingListener != null || extraListener != null) {
            setSpinnerInverseBindingListener(inverseBindingListener, extraListener)
        }
    }

    companion object InverseSpinnerBindings {

        @JvmStatic
        @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
        fun Spinner.getSelectedValue(): Any? {
            return getSpinnerItemId()
        }

    }

}

interface SpinnerItemRule {
    fun onSelectedValueChanged(item: SpinnerItem<Any>?)
}

private val currencySpinnerItems =
    listOf(CurrencySpinnerItem("", "Select a currency", false)) + listOf(
        "HUF", "EUR", "RON", "USD"
    ).map {
        CurrencySpinnerItem(it, it, true)
    }