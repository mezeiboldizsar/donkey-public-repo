package com.donkeyapp.donkey.bindings

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.donkeyapp.donkey.R
import com.donkeyapp.donkey.store.firebase.model.Money
import com.donkeyapp.donkey.utils.toLocalDate
import com.donkeyapp.donkey.utils.toLocalTime
import com.google.firebase.Timestamp
import org.threeten.bp.format.DateTimeFormatter
import java.text.DecimalFormat

class TextViewBinding {

    @BindingAdapter("setDate")
    fun TextView.setDate(date: Timestamp?) {
        if (date != null) {
            text = date.toLocalDate().toString()
        }
    }

    @BindingAdapter("setTime")
    fun TextView.setTime(date: Timestamp?) {
        if (date != null) {
            text = DateTimeFormatter.ofPattern("HH:mm").format(date.toLocalTime())
        }
    }

    @BindingAdapter("setMoney")
    fun TextView.setMoney(money: Money?) {
        if (money != null) {
            text = context.getString(
                R.string.money,
                numberFormatter.format(money.amount),
                money.currency
            )
        }
    }

    companion object {
        private val numberFormatter = DecimalFormat("#,###.##")
    }

}