package com.donkeyapp.donkey.bindings

import android.widget.SearchView
import androidx.databinding.BindingAdapter

class SearchViewBinding {

    @BindingAdapter("onChange")
    fun SearchView.setOnChange(searchRule: SearchRule?) {
        if (searchRule != null) {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(p0: String?): Boolean {
                    searchRule.search(p0 ?: return false)
                    return true
                }
            })
        }
    }

    @BindingAdapter("onSearchClickListener")
    fun SearchView.setOnSearchClickListener(rule: BaseRule?) {
        if (rule != null) {
            setOnSearchClickListener {
                rule.invoke()
            }
        }
    }

    @BindingAdapter("onCloseListener")
    fun SearchView.setOnCloseListener(rule: BaseRule?) {
        if (rule != null) {
            setOnCloseListener {
                rule.invoke()
                false
            }
        }
    }

}

interface SearchRule {
    fun search(s: String)
}

interface BaseRule {
    fun invoke()
}