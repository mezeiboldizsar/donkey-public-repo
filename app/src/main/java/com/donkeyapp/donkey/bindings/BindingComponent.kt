package com.donkeyapp.donkey.bindings

import androidx.databinding.DataBindingComponent

class BindingComponent : DataBindingComponent {

    override fun getSpinnerBindings() = SpinnerBindings()

    override fun getEditTextBinding() = EditTextBinding()

    override fun getRecyclerViewBinding() = RecyclerViewBinding()

    override fun getSearchViewBinding() = SearchViewBinding()

    override fun getTextViewBinding() = TextViewBinding()

    override fun getViewBinding() = ViewBinding()

}