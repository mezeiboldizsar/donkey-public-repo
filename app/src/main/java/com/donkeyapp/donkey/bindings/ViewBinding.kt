package com.donkeyapp.donkey.bindings

import android.view.View
import androidx.databinding.BindingAdapter
import com.donkeyapp.donkey.utils.Savable
import com.donkeyapp.donkey.utils.SaveState
import com.donkeyapp.donkey.utils.SwipeListener

class ViewBinding {

    @BindingAdapter("android:enabled")
    fun View.setEnabled(state: SaveState?) {
        if (state != null) {
            isEnabled = state == Savable
        }
    }

    @BindingAdapter("android:visibility")
    fun View.setVisibility(isVisible: Boolean?) {
        if (isVisible != null) {
            visibility = if (isVisible) View.VISIBLE else View.GONE
        }
    }

    @BindingAdapter(value = ["onRightSwipe", "onLeftSwipe"], requireAll = false)
    fun View.onSwipe(onRightSwipe: SwipeRule?, onLeftSwipe: SwipeRule?) {
        setOnTouchListener(object : SwipeListener() {
            override fun onRightSwipe() {
                onRightSwipe?.swipe()
            }
            override fun onLeftSwipe() {
                onLeftSwipe?.swipe()
            }
        })
    }

}

interface SwipeRule {
    fun swipe()
}