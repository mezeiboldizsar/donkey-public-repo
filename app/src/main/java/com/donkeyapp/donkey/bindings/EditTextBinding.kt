package com.donkeyapp.donkey.bindings

import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.donkeyapp.donkey.utils.afterTextChanged
import com.donkeyapp.donkey.utils.toDoubleOrZero
import java.text.DecimalFormat

class EditTextBinding {

    @BindingAdapter(value = ["doubleValueAttrChanged", "validate"], requireAll = false)
    fun EditText.validate(listener: InverseBindingListener?, validation: ValidationRule?) {
        afterTextChanged {
            if (validation != null) {
                val errorStringId = validation.validate(it)
                if (errorStringId != 0) {
                    error = this@validate.context.getString(errorStringId)
                }
            }
            listener?.onChange()
        }
    }

    @BindingAdapter(value = ["doubleValueNoZeroAttrChanged", "validate"], requireAll = false)
    fun EditText.validateNoZero(listener: InverseBindingListener?, validation: ValidationRule?) {
        afterTextChanged {
            if (validation != null) {
                val errorStringId = validation.validate(it)
                if (errorStringId != 0) {
                    error = this@validateNoZero.context.getString(errorStringId)
                }
            }
            listener?.onChange()
        }
    }

    @BindingAdapter("doubleValue")
    fun EditText.setDouble(value: Double?) {
        if (value != null) {
            val stringValue = numberFormatter.format(value)
            setText(stringValue)
            setSelection(stringValue.length)
        }
    }

    @BindingAdapter("doubleValueNoZero")
    fun EditText.setDoubleNoZero(value: Double?) {
        if (value != null) {
            if (value == 0.0) {
                setText("")
            } else {
                val stringValue = numberFormatter.format(value)
                setText(stringValue)
                setSelection(stringValue.length)
            }
        }
    }

    companion object {

        @InverseBindingAdapter(attribute = "doubleValue")
        @JvmStatic
        fun EditText.getDouble(): Double = text.toString().toDoubleOrZero()

        @InverseBindingAdapter(attribute = "doubleValueNoZero")
        @JvmStatic
        fun EditText.getDoubleNoZero(): Double = text.toString().toDoubleOrZero()

        private val numberFormatter = DecimalFormat("#,###.##")

    }

}

interface ValidationRule {
    fun validate(s: String): Int
}