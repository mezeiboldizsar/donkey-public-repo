package com.donkeyapp.donkey.bindings

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.donkeyapp.donkey.utils.replace
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter

class RecyclerViewBinding {

    @BindingAdapter("data")
    fun RecyclerView.data(groups: List<Group>?) {
        if (groups != null) {
            (adapter as? GroupAdapter<*>)?.replace(groups)
        }
    }

}