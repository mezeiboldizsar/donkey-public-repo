package com.donkeyapp.donkey.utils

inline fun withRetry(count: Int = 5, block: () -> Unit) {
    repeat(count) {
        try {
            block()
            return
        } catch (ex: Throwable) {
            print("Assertion failed, wait 1000 millis then retry.")
            if (it < count - 1) {
                Thread.sleep(1000)
            } else {
                throw ex
            }
        }
    }
}